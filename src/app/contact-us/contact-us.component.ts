import { Component, OnInit } from "@angular/core";

import { LoginService } from "../login.service";

import { LoaderService } from "../loader.service";
import {
  FormBuilder,
  FormGroup,
  FormControlName,
  FormControl
} from "@angular/forms";

@Component({
  selector: "app-contact-us",
  templateUrl: "./contact-us.component.html",
  styleUrls: ["./contact-us.component.css"]
})
export class ContactUsComponent implements OnInit {
  person;
  constructor(
    private loginService: LoginService,
    private spinner: LoaderService
  ) {
    // this.person = new FormGroup({
    // username: new FormControl(),
    // email: new FormControl(),
    // mobilenumber:new FormControl(),
    // subject:new FormControl(),
    // message:new FormControl(),
    // });
  }

  ngOnInit() {}
  submitContactdetails() {
    var username = document.forms["contactForm"]["username"];
    var email = document.forms["contactForm"]["email"];
    var mobile = document.forms["contactForm"]["mobile"];
    var subject = document.forms["contactForm"]["subject"];
    var message = document.forms["contactForm"]["message"];
    console.log(username.value);
    console.log(email.value);
    console.log(mobile.value);
    console.log(subject.value);

    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (username.value == "") {
      window.alert("Please enter your name.");
      username.focus();
      return false;
    }

    if (email.value == "") {
      window.alert("Please enter your email.");
      email.focus();
      return false;
    }

    if (!re.test(String(email.value).toLowerCase())) {
      window.alert("Enter correct email");
      email.focus();
      return false;
    }

    if (mobile.value == "") {
      window.alert("Please enter your mobile number.");
      mobile.focus();
      return false;
    }

    if (!mobile.value.match(/^(\+\d{1,3}[- ]?)?\d{10}$/)) {
      window.alert("Enter correct mobile number");
      mobile.focus();
      return false;
    }

    if (subject.value == "") {
      window.alert("Please enter your subject");
      subject.focus();
      return false;
    }

    if (message.value == "") {
      window.alert("Please enter your message.");
      message.focus();
      return false;
    } else {
      this.spinner.show();
      this.loginService
        .submitContactDetails(
          username.value,
          email.value,
          mobile.value,
          subject.value,
          message.value
        )
        .subscribe(reponse => {
          console.log(reponse);
          window.alert("Done");
        });
      return true;
    }
  }
}
