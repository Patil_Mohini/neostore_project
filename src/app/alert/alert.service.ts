import { Injectable } from '@angular/core';
import {Observable,Subject} from 'rxjs'
import{ filter } from 'rxjs/operators'
import { Alert, AlertType } from './alert.model';

import {Router,NavigationStart} from '@angular/router'
@Injectable({
  providedIn: 'root'
})
export class AlertService {
  private subject = new Subject<Alert>();
  private keepAfterRouteChange = false;
  data:any;
  constructor(private router: Router) {
// clear alert messages on route change unless 'keepAfterRouteChange' flag is true
this.router.events.subscribe(event => {
  if (event instanceof NavigationStart) {
      if (this.keepAfterRouteChange) {
          // only keep for a single route change
          this.keepAfterRouteChange = false;
      } else {
          // clear alert messages
          this.clear();
      }
  }
});

   }
  onAlert(alertId?: string): Observable<Alert> {
    return this.subject.asObservable().pipe(filter(x => x && x.alertId === alertId));
}

info(message, alertId) {
  console.log("2");
  this.alert(new Alert({ message, type:AlertType.Info,alertId }));
  //this.alert(message,alertId)
}
alert(alert:Alert) {
  console.log("3");
  console.log("11");
  this.keepAfterRouteChange = alert.keepAfterRouteChange;
   this.subject.next(alert);
}

// clear alerts
clear(alertId?: string) {
  this.subject.next(new Alert({ alertId }));
}

}
