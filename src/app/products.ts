export interface Products {
    
    _id: number,
    product_id: string,
    product_name:string,
    product_image: string,
    product_desc: string,
    product_rating: string,
    product_producer:string,
    product_stock: number,
    product_dimension:string ,
    product_material: string
    // "createdAt": "2019-06-20T06:35:36.356Z",
    // "__v": 0,
    // "subImages": [
    //   {
    //     "_id": "5d0b28917eb506755877867b",
    //     "product_subImages": [
    //       "2019-06-20T06-32-49.680Z319TbgBtsdL.jpg",
    //       "2019-06-20T06-32-49.682Z71pLyub4HIL._SL1500_.jpg",
    //       "2019-06-20T06-32-49.689Z41p7q6hYZRL.jpg"
    //     ],
    //     "subImages_id": "5d0b28917eb506755877867b",
    //     "__v": 0
    //   }
    // ],
    // "product_color": [
    //   {
    //     "_id": "5cfe247ade89f8148fbd0146",
    //     "color_name": "Rust Brown",
    //     "color_code": "#8B320B",
    //     "color_id": "5cfe247ade89f8148fbd0146",
    //     "__v": 0
    //   }
    // ],
    // "product_category
    
}
