import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { environment } from "../../environments/environment";
import { AlertService } from "../alert/alert.service";
import { CartService } from "../cart/cart.service";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from "@angular/material/dialog";
import { DialogBoxComponent } from "../dialog-box/dialog-box.component";
import { LoginService } from "../login.service";

@Component({
  selector: "app-product-detail",
  templateUrl: "./product-detail.component.html",
  styleUrls: ["./product-detail.component.css"]
})
export class ProductDetailComponent implements OnInit {
  productID;
  productsDetailArray;
  detail: Array<any>;
  imageId;
  configUrl;
  animal: string;
  name: string;
  rate: number;

  dataArray = [];

  constructor(
    private activateroute: ActivatedRoute,
    private alertService: AlertService,
    private cartService: CartService,
    public dialog: MatDialog,
    private loginService: LoginService
  ) {
    this.configUrl = environment.apiURl;
    console.log(this.configUrl);
  }

  ngOnInit() {
    this.productID = this.activateroute.snapshot.paramMap.get("id");
    console.log("id", this.productID);
    //onsole.log(JSON.parse(this.productsDetailArray));
    this.loginService.getProdDetailsByID(this.productID).subscribe(result => {
      console.log(result);
      this.productsDetailArray = result;
      this.productsDetailArray = this.productsDetailArray.product_details[0];

      this.imageId = this.productsDetailArray.product_image;
      console.log(this.imageId);
      console.log(this.configUrl + this.productsDetailArray.product_image);
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: "350px",
      height: "250px",

      data: { name: this.name, rate: this.rate }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("The dialog was closed");
      this.rate = result;
      this.updaterating();
      //this.loginService.updaterating(this.rate,this.productsDetailArray.product_id)
      console.log("rate", this.rate);
    });
  }
  updaterating() {
    this.loginService
      .updaterating(this.rate, this.productsDetailArray.product_id)
      .subscribe(result => {
        console.log("updaterating");
        console.log(result);
      });
  }

  LoadImage(c) {
    this.imageId = c;
  }

  addtoCart(productsDetailArray) {
    this.cartService.addItemsToCart(productsDetailArray);
  }

  info(message, alertId) {
    console.log("1");
    this.alertService.info(message, alertId);
  }

  openFacebook() {
    window.open("https://www.facebook.com/sharer/sharer.php?u=#url");
  }

  // $("#zoom_02").ezPlus({
  //   tint: true,
  //   tintColour: '#F90', tintOpacity: 0.5
  // });
}
