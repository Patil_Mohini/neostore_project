import { Component, OnInit } from '@angular/core';
import {LoginService} from '../login.service'
import { defaultOptions } from 'ngx-extended-pdf-viewer';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
companyDetails:any;
TermsAndConditions:any;
pdf:any;
data:any;
  constructor(private loginService:LoginService) { 
    defaultOptions.workerSrc = './assets/pdf.worker-es5.js';
  }
  getTermsandCondition(){
    
      this.loginService.getTermsandCondition().subscribe((data: any) => {
      this.pdf = data.termsAndConditions_details[0].fileName;
      window.open("http://180.149.241.208:3022/"+ this.pdf);
      });
  }
  getGuaranteeReturnPolicy(){
    this.loginService.getGuaranteePolicy().subscribe((data: any) => {
      this.pdf = data.guarantee_details[0].fileName;
      window.open("http://180.149.241.208:3022/"+ this.pdf);
      });

  }

  ngOnInit() {
    this.loginService.getComapanyDetails().subscribe((result) =>{
      console.log(result)
       this.data=result;
       this.companyDetails=this.data.company_details;

    })
    this.loginService.getTermsandCondition().subscribe((result)=>{
      console.log(result)
      this.data=result;
      this.TermsAndConditions=this.data.termsAndConditions_details[0].fileName;
      console.log(this.TermsAndConditions)
    }
    )



      
    } 
  }



