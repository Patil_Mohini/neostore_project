import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { MessageService } from "../message.service";
import { LoginService } from "../login.service";

@Injectable({
  providedIn: "root"
})
export class CartService {
  data;
  dataArray = [];
  index;
  cartcount;
  addTocart;
  sendData = [];
  flag;
  finalArray = [];
  result;
  private totalItems: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  constructor(
    private messageService: MessageService,
    private loginService: LoginService
  ) {}
  getCartItems(i) {
    return this.totalItems.asObservable();
  }

  updateCartItems(items: number) {
    this.totalItems.next(items);
  }

  addItemsToCart(productsDetailArray) {
    this.data = {};
    this.data.product_image = productsDetailArray.product_image;
    this.data.product_name = productsDetailArray.product_name;
    this.data.product_producer = productsDetailArray.product_producer;
    this.data.product_cost = productsDetailArray.product_cost;
    this.data.product_id = productsDetailArray.product_id;
    this.data.product_qty = 1;
    this.data.product_total_cost =
      this.data.product_qty * this.data.product_cost;
    console.log("data.product_total_cost", this.data.product_total_cost);
    if (localStorage.getItem("cartDetails")) {
      this.dataArray = JSON.parse(localStorage.getItem("cartDetails"));
      this.index = -1;
      for (var i = 0; i < this.dataArray.length; i++) {
        if (this.dataArray[i].product_id == this.data.product_id) {
          this.index = i;
          alert("Already added");
          break;
        }
      }
      if (this.index == -1) {
        this.dataArray.push(this.data);
        localStorage.setItem(
          "cartcount",
          JSON.stringify(this.dataArray.length)
        );
        this.cartcount = localStorage.getItem("cartcount");
        console.log("cartServicecalled", this.cartcount);
        this.messageService.setCount(this.cartcount);

        localStorage.setItem("cartDetails", JSON.stringify(this.dataArray));
        alert("Successfully added");
        this.dataArray = [];
        this.adddatatoCart();
      }

      console.log(this.dataArray);
    } else {
      this.dataArray.push(this.data);
      localStorage.setItem("cartDetails", JSON.stringify(this.dataArray));
      localStorage.setItem("cartcount", JSON.stringify(this.dataArray.length));
      this.cartcount = localStorage.getItem("cartcount");
      console.log("cartServicecalled", this.cartcount);
      this.messageService.setCount(this.cartcount);
      console.log("this.productsDetailArray", productsDetailArray);
      console.log("this.productsDetailArray", this.data);
      alert("Successfully added");
      this.dataArray = [];
      this.adddatatoCart();
    }
  }

  //adddatatoCart(data) {}
  adddatatoCart() {
    this.dataArray = JSON.parse(localStorage.getItem("cartDetails"));
    this.addTocart = {};
    this.finalArray = [];
    for (var j = 0; j < this.dataArray.length; j++) {
      this.addTocart = {};
      this.addTocart._id = this.dataArray[j].product_id;
      this.addTocart.quantity = this.dataArray[j].product_qty;
      this.finalArray.push(this.addTocart);
    }
    this.flag = {
      flag: "logout"
    };
    this.finalArray.push(this.flag);
    console.info("[cart.component.ts] this.dataArray =====>", this.finalArray);
    this.loginService
      .addProductToCartCheckout(this.finalArray)
      .subscribe(result => {
        console.log(result);
        this.finalArray = [];
        this.addTocart = {};
      });
  }
}
