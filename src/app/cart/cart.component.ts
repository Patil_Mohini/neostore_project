import { Component, OnInit } from "@angular/core";
import { environment } from "../../environments/environment";
import { CartService } from "./cart.service";
import { LoginService } from "../login.service";
import { Router } from "@angular/router";
import { MessageService } from "../message.service";

@Component({
  selector: "app-cart",
  templateUrl: "./cart.component.html",
  styleUrls: ["./cart.component.css"]
})
export class CartComponent implements OnInit {
  configUrl;
  cartDetails;
  qty;
  indexI;
  isdata: boolean = true;
  isNodata: boolean = false;
  subtotal;
  Gst;
  AddressDetails;
  orderTotal;
  result;
  dataArray = [];
  flag;
  addTocart;
  finalArray = [];
  product_id;
  public counter: number = 1;
  constructor(
    private cartService: CartService,
    private loginService: LoginService,
    private route: Router,
    private messageService: MessageService
  ) {
    this.configUrl = environment.apiURl;
  }
  increment(productid) {
    console.log("increment");
    console.log(productid);
    this.indexI = -1;
    for (var i = 0; i <= this.cartDetails.length; i++) {
      if (this.cartDetails[i].product_id == productid) {
        this.indexI = i;

        this.qty = this.cartDetails[i].product_qty;
        localStorage.setItem("cartDetails", this.cartDetails);
        break;
      }
    }
    if (!(this.indexI == -1)) {
      this.counter = this.cartDetails[this.indexI].product_qty;
      this.counter += 1;
      this.cartDetails[this.indexI].product_qty = this.counter;
      localStorage.setItem("cartDetails", JSON.stringify(this.cartDetails));
    }
    this.changeSubtotal();
  }

  decrement(productid) {
    console.log("decrement");
    console.log(productid);
    for (var i = 0; i <= this.cartDetails.length; i++) {
      if (this.cartDetails[i].product_id == productid) {
        this.indexI = i;

        this.qty = this.cartDetails[i].product_qty;
        localStorage.setItem("cartDetails", this.cartDetails);
        break;
      }
    }
    if (!(this.indexI == -1) && this.counter > 1) {
      this.counter = this.cartDetails[this.indexI].product_qty;
      this.counter -= 1;
      this.cartDetails[this.indexI].product_qty = this.counter;
      localStorage.setItem("cartDetails", JSON.stringify(this.cartDetails));
    }
    this.changeSubtotal();
    //this.cartService.updateCartItems(this.qty-1);

    // if(this.counter >1){
    //       this.counter -= 1;
    //       this.cartDetails[this.indexI].product_qty=this.counter;
    //       console.log("increment",JSON.stringify(this.cartDetails));
    //       localStorage.setItem("cartDetails",JSON.stringify(this.cartDetails));
    //   }
  }

  deleteItem(removeid) {
    console.log(removeid);
    if (localStorage.getItem("cartDetails")) {
      for (var i = 0; i < this.cartDetails.length; i++) {
        if (this.cartDetails[i].product_id == removeid) {
          this.product_id = this.cartDetails[i].product_id;
          this.cartDetails.splice(i, 1);

          console.info(
            "[cart.component.ts] this.cartDetails  deleteitem =====>",
            this.cartDetails
          );

          //localStorage["cartDetails"] = this.cartDetails;
          localStorage.setItem("cartDetails", JSON.stringify(this.cartDetails));
          console.info(
            "[cart.component.ts] this.cartDetails =====>",
            this.cartDetails.length
          );
          localStorage.setItem("cartcount", this.cartDetails.length);
          this.messageService.setCount(JSON.stringify(this.cartDetails.length));
          this.loginService
            .deleteCartProduct(this.product_id)
            .subscribe(result => {
              console.log(result);
              console.info(
                "[cart.component.ts]deleteCartProduc =====>",
                result
              );
            });

          if (this.cartDetails.length > 0) {
            this.isdata = true;
            this.isNodata = false;
            this.changeSubtotal();
          } else {
            this.isdata = false;
            this.isNodata = true;
          }
        }
      }
    }
  }

  changeSubtotal() {
    this.subtotal = 0;
    for (var i = 0; i < this.cartDetails.length; i++) {
      this.subtotal +=
        this.cartDetails[i].product_qty * this.cartDetails[i].product_cost;
    }
    this.Gst = Math.round(0.05 * this.subtotal);
    this.orderTotal = this.subtotal + this.Gst;
  }
  getAddress() {
    this.loginService.getAddress().subscribe(result => {
      console.log(result);
      this.result = result;
      this.AddressDetails = this.result.customer_address;

      console.log(this.AddressDetails);
      // this.AddressDetails = this.AddressDetails.customer_address[0];
      // console.log(this.AddressDetails.customer_address[0]);
      //this.data=result;
    });
  }

  editAddress(AddressDetails) {
    console.log(AddressDetails);
    this.route.navigate([
      "Address",
      { data: JSON.stringify(AddressDetails[0]) }
    ]);

    //this.routes.navigate(['productDetail',{data:JSON.stringify(this.data.product_details[0])}]);
  }

  enablebutton(value) {
    console.log(value);
  }

  placeOrder() {
    this.dataArray = JSON.parse(localStorage.getItem("cartDetails"));

    this.finalArray = [];
    for (var j = 0; j < this.dataArray.length; j++) {
      this.addTocart = {};
      this.addTocart._id = this.dataArray[j].product_id;
      this.addTocart.quantity = this.dataArray[j].product_qty;
      this.addTocart.product_id = this.dataArray[j].product_id;
      this.finalArray.push(this.addTocart);
    }
    this.flag = {
      flag: "checkout"
    };
    this.finalArray.push(this.flag);
    console.info("[cart.component.ts] this.dataArray =====>", this.finalArray);
    this.loginService
      .addProductToCartCheckout(this.finalArray)
      .subscribe(result => {
        console.log(result);
        this.result = result;
        if (this.result.success == true) {
          this.route.navigate(["orderplaced"]);
        }
        this.finalArray = [];
        this.addTocart = {};
      });
    localStorage.removeItem("cartDetails");
    this.messageService.setCount("0");
  }
  ngOnInit() {
    this.qty = "1";
    console.log(this.qty);
    this.getAddress();
    if (localStorage.getItem("cartDetails")) {
      this.cartDetails = localStorage.getItem("cartDetails");
      this.cartDetails = JSON.parse(this.cartDetails);
      console.log("length");
      console.log(this.cartDetails.length);
      this.changeSubtotal();
    } else {
      this.isdata = false;
      this.isNodata = true;
    }

    // if (this.cartDetails.length > 0) {
    //   this.isdata = true;
    //   this.isNodata = false;
    // } else {
    //   this.isdata = false;
    //   this.isNodata = true;
    // }
    //console.log("cartDetails", JSON.parse(this.cartDetails));
  }
}
