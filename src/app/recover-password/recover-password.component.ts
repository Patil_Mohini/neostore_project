import { Component, OnInit } from "@angular/core";
import { LoginService } from "../login.service";

@Component({
  selector: "app-recover-password",
  templateUrl: "./recover-password.component.html",
  styleUrls: ["./recover-password.component.css"]
})
export class RecoverPasswordComponent implements OnInit {
  name;
  email;
  constructor(private loginService: LoginService) {}

  ngOnInit() {}
  submitContactdetails() {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (this.email) {
      if (!re.test(String(this.email).toLowerCase())) {
        alert("Enter correct email");
      } else {
        this.loginService.forgetPassword(this.email).subscribe(result => {
          console.log(result);
        });
      }
    } else {
      alert("Enter email");
    }
  }
}
