import { TestBed } from '@angular/core/testing';

import { NeostoreServicesService } from './neostore-services.service';

describe('NeostoreServicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NeostoreServicesService = TestBed.get(NeostoreServicesService);
    expect(service).toBeTruthy();
  });
});
