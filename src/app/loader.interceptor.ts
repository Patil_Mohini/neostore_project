import { Injectable } from "@angular/core";
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { map, finalize, catchError } from "rxjs/operators";
import { LoaderService } from "./loader.service";
@Injectable()
export class LoaderInterceptor implements HttpInterceptor {
  token;
  constructor(public loaderService: LoaderService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (localStorage.getItem("CustomerDetails")) {
      this.token = JSON.parse(localStorage.getItem("CustomerDetails"));
      console.log("authid", this.token.token);
      this.token = this.token.token;
      // this.headers.append("Authorization","Bearer"+JSON.stringify(this.authId.token));
      //    this.httpOptions ={
      //      headers: new HttpHeaders({
      //        'Content-Type':  'application/json',
      //        'Authorization': this.authId
      //      })
      //    };
    }
    if (this.token) {
      req = req.clone({
        headers: req.headers.set("Authorization", "Bearer " + this.token)
      });
    }

    if (!req.headers.has("Content-Type")) {
      req = req.clone({
        headers: req.headers.set("Content-Type", "application/json")
      });
    }

    req = req.clone({ headers: req.headers.set("Accept", "application/json") });
    this.loaderService.show();
    console.log("interceptor loader show");
    return next.handle(req).pipe(
      //finalize(() => this.loaderService.hide()),
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          console.log("event", event);
          this.loaderService.hide();
          //alert(event.body.message);
        }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        let data = {};
        data = {
          message: error.message
        };

        console.log(error);
        this.loaderService.hide();
        alert(error.error.message);

        return throwError(error);
      })
    );
  }
}
