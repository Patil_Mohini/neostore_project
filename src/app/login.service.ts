import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { EmailValidator } from "@angular/forms";
import { environment } from "../environments/environment";
import { Products } from "./products";
import { Condition } from "selenium-webdriver";

const configUrl = environment.apiURl;
@Injectable({
  providedIn: "root"
})
export class LoginService {
  category_id;
  url;
  sortbyval;
  sortinval;
  authId;
  headers;
  httpOptions;
  constructor(private http: HttpClient) {
    //this.headers = new HttpHeaders();
    // if(localStorage.getItem("CustomerDetails")){
    //   this.authId=JSON.parse(localStorage.getItem("CustomerDetails"));
    //   console.log("authid",this.authId.token);
    //   this.authId="bearer "+this.authId.token;
    //  // this.headers.append("Authorization","Bearer"+JSON.stringify(this.authId.token));
    //  this.httpOptions = {
    //    headers: new HttpHeaders({
    //      'Content-Type':  'application/json',
    //      'Authorization': this.authId
    //    })
    //  };
    // }
  }

  /**
   * // TODO: comment login
   * Gets script version
   * @param email ,pass
   * @returns
   */
  authlogin(email, passw): Observable<any> {
    return this.http.post<any>(configUrl + "login", {
      email: email,
      pass: passw
    });
    // .pipe(
    //   catchError(this.handleError('addHero', any))
    // );
  }

  // register (email,pass,fname,lname,passconfirm,Contactnumber): Observable<any>{
  // return this.http.post<any>(configUrl + 'register',{"first_name":fname,"last_name":lname, "email":email ,"pass": pass,"confirmPass" :passconfirm,"phone_no":Contactnumber})
  // }
  /**
   * // TODO: comment register
   * Gets script version
   * @param first_name ,last_name,email,pass,passconfirm,phone_no
   * @returns
   */
  register(registerdetails): Observable<any> {
    console.log("registerdetails");
    console.log(registerdetails);
    return this.http.post<any>(configUrl + "register", {
      first_name: registerdetails.fname,
      last_name: registerdetails.lname,
      email: registerdetails.email,
      pass: registerdetails.pass,
      confirmPass: registerdetails.passconfirm,
      phone_no: registerdetails.Contactnumber,
      gender: registerdetails.gender
    });
  }

  // getTopProducts(): Observable<any> {

  //   return this.http.get<any>(configUrl+'defaultTopRatingProduct' )

  // }

  /**
   * // TODO: comment defaultTopRatingProduct
   * Gets script version
   * @param no params passed
   * @returns
   */
  getTopProducts() {
    return this.http.get(configUrl + "defaultTopRatingProduct");
  }

  getAllProducts() {
    return this.http.get(configUrl + "getAllProducts");
  }
  /**
   * // TODO: comment getAllCategories
   * Gets script version
   * @param no params passed
   * @returns
   */
  getAllcategories() {
    return this.http.get(configUrl + "getAllCategories");
  }

  /**
   * // TODO: comment getData
   * Gets script version
   * @param no params passed
   * @returns
   */
  getComapanyDetails() {
    return this.http.get(configUrl + "getData");
  }

  /**
   * // TODO: comment getTermsAndConditions
   * Gets script version
   * @param no params passed
   * @returns
   */
  getTermsandCondition() {
    return this.http.get(configUrl + "getTermsAndConditions");
  }

  /**
   * // TODO: comment getGuarantee
   * Gets script version
   * @param no params passed
   * @returns
   */
  getGuaranteePolicy() {
    return this.http.get(configUrl + "getGuarantee");
  }

  // submitContactDetails(email,name,sub,phoneno,message){
  //   return this.http.post<any>(configUrl + 'contactUs',{"email":email,"name":name, "subject":sub ,"phone_no": phoneno,"message" :message})
  // }

  /**
   * // TODO: comment contactUs
   * Gets script version
   * @param name,email,subject,phone_no,message
   * @returns
   */
  submitContactDetails(username, email, mobile, subject, message) {
    return this.http.post<any>(configUrl + "contactUs", {
      email: email,
      name: username,
      subject: subject,
      phone_no: mobile,
      message: message
    });
  }

  getproductbyCatid(id) {
    console.log("categoryid", id);

    this.url = configUrl + "getProductByCateg/" + id;
    console.log("categoryid", this.url);

    return this.http.get<any>(this.url);
  }

  getProductBycolorid(colorid) {
    this.url = configUrl + "getProductByCateg/" + colorid;
    console.log("categoryid", this.url);
    return this.http.get<any>(this.url);
  }
  getAllcolors() {
    return this.http.get(configUrl + "getAllColors");
  }

  getCommonProducts(catIId, colorId, productid, sortby, sortin) {
    if (sortby != "" && sortby != undefined) {
      this.sortbyval = sortby;
    } else {
      this.sortbyval = "";
    }
    if (sortin != "" && sortin != undefined) {
      this.sortinval = sortin;
    } else {
      this.sortinval = "";
    }

    // this.url=configUrl+"commonProducts?"+filterType+'='+id;
    this.url =
      configUrl +
      "commonProducts?" +
      "_id=" +
      productid +
      "&category_id=" +
      catIId +
      "&color_id=" +
      colorId +
      "&sortBy=" +
      this.sortbyval +
      "&sortIn=" +
      this.sortinval +
      "&name=" +
      "";
    // http://180.149.241.208:3022/commonProducts?category_id=&color_id=&sortBy=&sortIn=&name=&pageNo=1&perPage=8
    console.log("categoryid", this.url);
    return this.http.get<any>(this.url);
  }

  updaterating(rate, productId) {
    console.log("updaterating");
    // this.url=configUrl+"updateProductRatingByCustomer/";
    console.log("rate", rate);
    console.log("productId", productId);
    return this.http.put<any>(configUrl + "updateProductRatingByCustomer", {
      product_id: productId,
      product_rating: rate
    });
  }
  getAddress() {
    return this.http.get(configUrl + "getCustAddress");
  }

  submitAddress(addressdetail): Observable<any> {
    console.log("registerdetails");
    console.log(addressdetail);
    return this.http.post<any>(configUrl + "address", {
      address: addressdetail.address,
      pincode: addressdetail.pincode,
      city: addressdetail.city,
      state: addressdetail.state,
      country: addressdetail.country
    });
  }

  forgetPassword(email) {
    return this.http.post<any>(configUrl + "forgotPassword", {
      email: email
    });
  }

  getProdDetailsByID(Id) {
    this.url = configUrl + "getProductByProdId/" + Id;
    console.log("getProdDetailsByID", this.url);

    return this.http.get<any>(this.url);
  }

  deleteCartProduct(product_id) {
    return this.http.delete(configUrl + "deleteCustomerCart/" + product_id);
  }

  addProductToCartCheckout(data) {
    return this.http.post<any>(configUrl + "addProductToCartCheckout", data);
  }

  getCartData() {
    return this.http.get(configUrl + "getCartData");
  }

  getOrderDetails() {
    return this.http.get(configUrl + "getOrderDetails");
  }
  private handleError(error: Response) {
    return Observable.throw(error.statusText);
  }
}
