import { Component, OnInit } from "@angular/core";
import { LoginService } from "../login.service";

@Component({
  selector: "app-my-account",
  templateUrl: "./my-account.component.html",
  styleUrls: ["./my-account.component.css"]
})
export class MyAccountComponent implements OnInit {
  result;
  AddressDetails;

  orderdetails;
  constructor(private loginService: LoginService) {}

  ngOnInit() {
    this.getAddress();
    this.getOrderDetails();
  }

  getAddress() {
    this.loginService.getAddress().subscribe(result => {
      console.log(result);
      this.result = result;
      //this.AddressDetails = this.result.customer_address;

      //console.log(this.AddressDetails);
      // this.AddressDetails = this.AddressDetails.customer_address[0];
      // console.log(this.AddressDetails.customer_address[0]);
      //this.data=result;
    });
  }

  getOrderDetails() {
    this.loginService.getOrderDetails().subscribe(result => {
      console.log(result);
      this.result = result;
      this.orderdetails = this.result.product_details;
    });
  }
}
