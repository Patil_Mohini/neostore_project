import { NgModule } from "@angular/core";
import {
  MatIconModule,
  MatMenuModule,
  MatToolbarModule,
  MatNativeDateModule,
  MatCardModule,
  MatFormFieldModule,
  MatSelectModule,
  MatProgressSpinnerModule,
  MatInputModule,
  MatButtonModule,
  MatDialogModule,
  MatTableModule,
  MatExpansionModule,
  MatListModule,
  MatTabsModule,
  MatStepperModule,
  MatGridListModule,
  MatRadioModule
} from "@angular/material";

@NgModule({
  imports: [
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatToolbarModule,
    MatNativeDateModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatTableModule,
    MatExpansionModule,
    MatListModule,
    MatTabsModule,
    MatStepperModule,
    MatGridListModule,
    MatRadioModule
  ],
  exports: [
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatToolbarModule,
    MatNativeDateModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatTableModule,
    MatExpansionModule,
    MatListModule,
    MatTabsModule,
    MatStepperModule,
    MatGridListModule,
    MatRadioModule
  ]
})
export class MaterialUI {}
