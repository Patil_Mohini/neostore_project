import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class MessageService {
  constructor() {}

  private subject = new Subject<any>();
  private loginSubject = new Subject<any>();

  // sendMessage(message: string) {
  //   this.subject.next({ text: message });
  // }
  setCount(cartcount: string) {
    console.log(cartcount);
    this.subject.next({ cartcount: cartcount });
  }

  getCount(): Observable<any> {
    return this.subject.asObservable();
  }

  setLoginStatus(isloggedin: string) {
    console.log(isloggedin);
    this.loginSubject.next({ isloggedin: isloggedin });
  }
  getLoginSatus(): Observable<any> {
    return this.loginSubject.asObservable();
  }

  clearCount() {
    this.subject.next();
  }

  // getMessage(): Observable<any> {
  //   return this.subject.asObservable();
  // }
}
