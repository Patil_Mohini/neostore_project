import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import {
  FormBuilder,
  FormGroup,
  FormControlName,
  FormControl
} from "@angular/forms";
import { LoginService } from "../login.service";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})
export class RegisterComponent implements OnInit {
  RegisterForm;
  constructor(
    private loginService: LoginService,
    private formBuilder: FormBuilder,
    private route: Router
  ) {
    this.RegisterForm = new FormGroup({
      email: new FormControl(),
      pass: new FormControl(),
      fname: new FormControl(),
      lname: new FormControl(),
      passconfirm: new FormControl(),
      Contactnumber: new FormControl(),
      gender: new FormControl()
    });
  }

  ngOnInit() {}

  onSubmit() {
    // this.loginService.register(this.RegisterForm.value.email,this.RegisterForm.value.pass,this.RegisterForm.value.fname,this.RegisterForm.value.lname,this.RegisterForm.value.passconfirm,this.RegisterForm.value.Contactnumber).subscribe((result) => {
    //   console.log(result);
    //   // email,pass,fname,lname,passconfirm,Contactnumber
    // })
    console.log(this.RegisterForm.value.gender);
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    // if (this.RegisterForm.value.fname == null) {
    //   alert("Please enter your first name.");
    //   // this.loginForm.email.focus();
    //   return false;
    // } else if (this.RegisterForm.value.lname == null) {
    //   alert("Please enter last name");
    //   //this.loginForm.password.focus();
    //   return false;
    // } else if (this.RegisterForm.value.email == null) {
    //   alert("Please enter password");
    //   //this.loginForm.password.focus();
    //   return false;
    // }
    if (!re.test(String(this.RegisterForm.value.email).toLowerCase())) {
      alert("Enter correct email");
      // this.loginForm.email.focus();
      return false;
    }
    //else if (this.RegisterForm.value.pass == null) {
    //   alert("Please enter password");
    //   //this.loginForm.password.focus();
    //   return false;
    // } else if (this.RegisterForm.value.passconfirm == null) {
    //   alert("Please enter  confirm password");
    //   //this.loginForm.password.focus();
    //   return false;
    // }
    else {
      this.loginService.register(this.RegisterForm.value).subscribe(result => {
        console.log(result);
        this.route.navigate(["login"]);
        // email,pass,fname,lname,passconfirm,Contactnumber
      });
    }
  }
}
