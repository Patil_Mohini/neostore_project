import { Component, OnInit } from "@angular/core";
import { LoginService } from "../login.service";
import { Products } from "../products";
import { environment } from "../../environments/environment";
import { Router } from "@angular/router";
import { CartService } from "../cart/cart.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  topRatedproductsArray: any;
  result: any;
  data: any;
  categoriesDetailsArray: any;
  Productlength: any;
  configUrl;
  constructor(
    private loginService: LoginService,
    private router: Router,
    private cartService: CartService
  ) {
    this.configUrl = environment.apiURl;
  }

  goToProductDetail(productid) {
    this.router.navigate(["/productDetail/", productid]);
  }

  ngOnInit() {
    // this.getProducts();
    this.loginService.getTopProducts().subscribe(result => {
      console.log(result);
      this.data = result;
      this.Productlength = this.data.product_details.length;

      this.topRatedproductsArray = this.data.product_details;
      console.log(
        this.data.product_details[0].DashboardProducts[0].product_image
      );
      console.log(
        this.data.product_details[1].DashboardProducts[0].product_image
      );
      console.log(
        this.data.product_details[2].DashboardProducts[0].product_image
      );
      console.log(
        this.data.product_details[3].DashboardProducts[0].product_image
      );

      //     console.log(JSON.stringify(result));
      //     result=JSON.stringify(result);
      //  this._postsArray=result;
      //
    });

    this.loginService.getAllcategories().subscribe(result => {
      console.log(result);
      this.data = result;

      this.categoriesDetailsArray = this.data.category_details;
      console.log("categoriesDetailsArray", this.categoriesDetailsArray);
    });
  }

  addtoCart(productsDetailArray) {
    this.cartService.addItemsToCart(productsDetailArray);
  }
  goToProductList(id, name) {
    console.log("id", id);
    console.log("name", name);
    this.router.navigate(["/productList", id, name]);
  }
}
