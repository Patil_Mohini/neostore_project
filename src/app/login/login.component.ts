import { Component, OnInit, EventEmitter, Output } from "@angular/core";
import { LoginService } from "../login.service";
import {
  FormBuilder,
  FormGroup,
  FormControlName,
  FormControl,
  EmailValidator
} from "@angular/forms";
import { Router } from "@angular/router";
import { MessageService } from "../message.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  // email:any;
  // password:any;
  loginForm;
  result;
  isLoggedin;
  count;
  dataArray = [];
  data;

  constructor(
    private loginService: LoginService,
    private formBuilder: FormBuilder,
    private route: Router,
    private messageService: MessageService
  ) {
    this.loginForm = new FormGroup({
      email: new FormControl(),
      password: new FormControl()
    });
  }

  //   this.myGroup = new FormGroup({
  //     firstName: new FormControl()
  //  });
  // }

  ngOnInit() {}

  onSubmit() {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (this.loginForm.value.email == "") {
      alert("Please enter your email.");
      // this.loginForm.email.focus();
      return false;
    } else if (!re.test(String(this.loginForm.value.email).toLowerCase())) {
      alert("Enter correct email");
      // this.loginForm.email.focus();
      return false;
    } else if (this.loginForm.value.password == "") {
      alert("Please enter password");
      //this.loginForm.password.focus();
      return false;
    } else {
      this.loginService
        .authlogin(this.loginForm.value.email, this.loginForm.value.password)
        .subscribe(result => {
          console.log(result);
          this.result = result;
          //localStorage.setItem("cartcount", this.result.cart_count);
          console.log(JSON.stringify(this.result));
          localStorage.setItem("CustomerDetails", JSON.stringify(this.result));
          if (this.result.success == true) {
            this.route.navigate([""]);
            localStorage.setItem("isLoggedin", "true");
            this.messageService.setLoginStatus(JSON.stringify("true"));
            this.loginService.getCartData().subscribe(result => {
              this.data = {};
              console.info(
                "[login.component.ts]loginService.getCartData()  =====>",
                result
              );
              this.result = result;
              if (this.result.product_details.length) {
                console.info(
                  "[login.component.ts]this.result  =====>",
                  this.result.product_details.length
                );
                this.count = this.result.product_details.length;
                localStorage.setItem("cartcount", JSON.stringify(this.count));
                for (var i = 0; i < this.count; i++) {
                  const product_details = this.result.product_details[i];
                  this.data = {};
                  this.data.product_image =
                    product_details.product_id.product_image;
                  this.data.product_name =
                    product_details.product_id.product_name;
                  this.data.product_producer =
                    product_details.product_id.product_producer;
                  this.data.product_cost =
                    product_details.product_id.product_cost;
                  this.data.product_id = product_details.product_id.product_id;
                  this.data.product_qty = 1;
                  this.data.product_total_cost =
                    this.data.product_qty * this.data.product_cost;
                  this.dataArray.push(this.data);
                }
                localStorage.setItem(
                  "cartDetails",
                  JSON.stringify(this.dataArray)
                );
                this.messageService.setCount(JSON.stringify(this.count));
              }
            });
          } else {
            alert(this.result.message);
            this.messageService.setLoginStatus(JSON.stringify("false"));
            this.isLoggedin = "false";

            localStorage.setItem("isLoggedin", "false");
          }
        });
    }
  }
}
