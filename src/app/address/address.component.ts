import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { LoginService } from "../login.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-address",
  templateUrl: "./address.component.html",
  styleUrls: ["./address.component.css"]
})
export class AddressComponent implements OnInit {
  addnewAddress;
  editAddress;
  heading;

  constructor(
    private loginservice: LoginService,
    private activateroute: ActivatedRoute
  ) {
    this.addnewAddress = new FormGroup({
      address: new FormControl(),
      pincode: new FormControl(),
      city: new FormControl(),
      state: new FormControl(),
      country: new FormControl()
    });
  }

  submitAddress() {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    this.loginservice
      .submitAddress(this.addnewAddress.value)
      .subscribe(result => {
        console.log(result);
      });
  }

  ngOnInit() {
    if (this.activateroute.snapshot.paramMap.get("data")) {
      this.heading = "Edit Address";
      this.editAddress = this.activateroute.snapshot.paramMap.get("data");
      console.log(JSON.parse(this.editAddress));
      this.editAddress = JSON.parse(this.editAddress);
      console.info(
        "[address.component.ts] this.editAddress  =====>",
        this.editAddress
      );
      //document.getElementById("city") = "new value";
      this.addnewAddress.setValue({
        address: this.editAddress.address,
        pincode: this.editAddress.pincode,
        city: this.editAddress.city,
        state: this.editAddress.state,
        country: this.editAddress.country
      });
    } else {
      this.heading = "Add new address";
    }
  }
}
