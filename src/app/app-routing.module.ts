import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ProductDetailComponent } from "./product-detail/product-detail.component";
import { FooterComponent } from "./footer/footer.component";
import { HeaderComponent } from "./header/header.component";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { ProductListComponent } from "./product-list/product-list.component";
import { OrderComponent } from "./order/order.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { ContactUsComponent } from "./contact-us/contact-us.component";
import { ProfileComponent } from "./profile/profile.component";
import { SubscribeComponent } from "./subscribe/subscribe.component";
import { RecoverPasswordComponent } from "./recover-password/recover-password.component";
import { CartComponent } from "./cart/cart.component";
import { AddressComponent } from "./address/address.component";
import { MyAccountComponent } from "./my-account/my-account.component";
import { ChangepasswordComponent } from "./changepassword/changepassword.component";
import { OrderPlacedComponent } from "./order-placed/order-placed.component";

const routes: Routes = [
  { path: "", component: DashboardComponent },
  { path: "login/Dashboard", component: DashboardComponent },

  { path: "login", component: LoginComponent },

  { path: "login/register", component: RegisterComponent },
  { path: "login/recoverpassword", component: RecoverPasswordComponent },

  { path: "productList", component: ProductListComponent },
  { path: "Dashboard/productList", component: ProductListComponent },
  { path: "productList/:id", component: ProductListComponent },
  { path: "productList/:id/:name", component: ProductListComponent },
  { path: "cart/productList", component: ProductListComponent },
  { path: "productDetail/:id", component: ProductDetailComponent },
  { path: "productDetail", component: ProductDetailComponent },
  //{ path: "Dashboard/productDetail/:id", component: ProductDetailComponent },

  { path: "order", component: OrderComponent },
  { path: "Dashboard", component: DashboardComponent },

  { path: "ContactUs", component: ContactUsComponent },
  // { path: "profile", component: ProfileComponent },
  { path: "subscribe", component: SubscribeComponent },

  { path: "cart", component: CartComponent },

  { path: "Address", component: AddressComponent },
  { path: "cart/Address", component: AddressComponent },

  { path: "cart/Address/profile", component: ProfileComponent },

  { path: "myaccount", component: MyAccountComponent },
  { path: "Address/myaccount", component: MyAccountComponent },
  //{ path: "cart/Address/myaccount", component: MyAccountComponent },

  { path: "changePassword", component: ChangepasswordComponent },
  { path: "orderplaced", component: OrderPlacedComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
