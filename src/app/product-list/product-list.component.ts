import { Component, OnInit } from "@angular/core";
import { LoginService } from "../login.service";
import { environment } from "../../environments/environment";
import { JwPaginationComponent } from "jw-angular-pagination";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { CartService } from "../cart/cart.service";

@Component({
  selector: "app-product-list",
  templateUrl: "./product-list.component.html",
  styleUrls: ["./product-list.component.css"]
})
export class ProductListComponent implements OnInit {
  data;
  Categories;
  configUrl;
  items = [];
  pageOfItems: Array<any>;
  categoriesDetailsArray: any;
  colorCategory: Array<any>;
  bgColor;
  filtertype;
  sub: any;
  id: any;
  name: any;
  CategoryName: any;
  colorid;
  categoryid;
  isdata: boolean = true;
  isNodata: boolean = false;
  constructor(
    private loginService: LoginService,
    private routes: Router,
    private activateroute: ActivatedRoute,
    private cartService: CartService
  ) {
    this.configUrl = environment.apiURl;
    // this.bgColor = 'BBFFFF';
  }

  ngOnInit() {
    // this.activateroute.params.subscribe(params => {
    //   this.id = params["id"];
    //   this.name = params["name"];
    //   console.log("productlist id", this.id);
    //   console.log("productlist name", this.name);
    // });
    this.id = this.activateroute.snapshot.paramMap.get("id");
    this.name = this.activateroute.snapshot.paramMap.get("name");
    console.log("productlist name", this.name);

    if (this.sub != "" && this.sub != undefined) {
      this.CategoryName = this.name;
    } else {
      this.CategoryName = "All Categories";
    }

    if (this.id != "" && this.id != undefined) {
      this.loadCategoryDatabyId(this.id, this.name);
    } else {
      this.getAllProducts();
    }

    this.loginService.getAllcategories().subscribe(result => {
      console.log(result);
      this.data = result;
      this.categoriesDetailsArray = this.data.category_details;
    });

    this.loginService.getAllcolors().subscribe(result => {
      console.log(result);
      this.data = result;
      this.colorCategory = this.data.color_details;
      this.bgColor = this.colorCategory[0].color_code;
      console.log(this.colorCategory[0].color_code);
    });
  }

  getAllProducts() {
    this.loginService.getAllProducts().subscribe(result => {
      console.log(result);
      this.data = result;
      this.Categories = this.data.product_details;
    });
  }
  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }

  loadCategoryDatabyId(id, name) {
    console.log("loadCategoryData", id);
    this.filtertype = "category_id";
    this.categoryid = id;
    this.loginService
      .getCommonProducts(id, "", "", "", "")
      .subscribe(result => {
        console.log(result);
        this.data = result;
        if (this.data.success == false) {
          this.isdata = false;
          this.isNodata = true;
        } else {
          this.isdata = true;
          this.isNodata = false;
        }
        this.Categories = this.data.product_details;
        this.CategoryName = this.Categories[0].category_id.category_name;
      });
    this.routes.navigate(["/productList", id, name]);
  }

  loadProductbyColor(colorid) {
    console.log("loadProductbyColor", colorid);
    this.filtertype = "color_id";
    this.colorid = colorid;
    this.loginService
      .getCommonProducts(this.categoryid, colorid, "", "", "")
      .subscribe(result => {
        console.log(result);
        this.data = result;
        if (this.data.success == false) {
          this.isdata = false;
          this.isNodata = true;
        } else {
          this.isdata = true;
          this.isNodata = false;
        }
        this.Categories = this.data.product_details;
      });
  }

  getSortProducts(rating, value) {
    this.loginService
      .getCommonProducts("", "", "", rating, value)
      .subscribe(result => {
        console.log(result);
        this.data = result;
        if (this.data.success == false) {
          this.isdata = false;
          this.isNodata = true;
        } else {
          this.isdata = true;
          this.isNodata = false;
        }
        this.Categories = this.data.product_details;
      });
  }

  getCommonProducts(categoryid, colorid) {
    console.log("loadCategoryData", categoryid);
    this.filtertype = "category_id";
    this.loginService
      .getCommonProducts(categoryid, colorid, "", "", "")
      .subscribe(result => {
        console.log(result);
        this.data = result;
        if (this.data.success == false) {
          this.isdata = false;
          this.isNodata = true;
        } else {
          this.isdata = true;
          this.isNodata = false;
        }
        this.Categories = this.data.product_details;
      });
  }
  getProductDetail(productid) {
    //console.log(this.data.productid);
    this.routes.navigate(["/productDetail/", productid]);
  }

  addtoCart(productsDetailArray) {
    this.cartService.addItemsToCart(productsDetailArray);
  }
}
