import {
  Component,
  ViewChild,
  AfterViewInit,
  OnInit,
  OnDestroy
} from "@angular/core";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { MessageService } from "../message.service";
import { FormControl, ReactiveFormsModule, FormBuilder } from "@angular/forms";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { ProductDetailComponent } from "../product-detail/product-detail.component";
import { ProductListComponent } from "../product-list/product-list.component";
import { LoginComponent } from "../login/login.component";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit, OnDestroy {
  show: boolean;
  visible;
  cartCount;
  username;
  message: any;
  subscription1: Subscription;
  subscription2: Subscription;

  constructor(private route: Router, private messageService: MessageService) {
    this.subscription1 = this.messageService.getCount().subscribe(a => {
      console.log("data retrieved", JSON.parse(a.cartcount));
      this.cartCount = JSON.parse(a.cartcount);
    });

    this.subscription2 = this.messageService.getLoginSatus().subscribe(a => {
      console.info(
        "[header.component.ts] getLoginSatus =====>",
        JSON.parse(a.isloggedin)
      );
      this.visible = JSON.parse(a.isloggedin);
      if (this.visible == "true") {
        this.show = true;
      }
    });
  }

  ngOnInit() {
    this.username = "";

    //this.visible=localStorage.getItem("isLoggedin");
    if (localStorage.length > 0) {
      console.log(localStorage);
      this.show = true;
    } else {
      console.log("else", localStorage);
      this.show = false;
    }

    if (localStorage.getItem("cartcount")) {
      this.cartCount = localStorage.getItem("cartcount");
    }

    //this.messageService.setCount(this.cartCount);

    console.info("[header.component.ts] this.cartCount =====>", this.cartCount);

    console.log("after messageService called");
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription1.unsubscribe();
    this.subscription2.unsubscribe();
  }
  onLogin(user: string) {
    this.username = user;
    console.log("this.username");
    console.log(this.username);
  }

  logout() {
    //localStorage.setItem("isLoggedin", "false");
    this.show = false;
    localStorage.removeItem("cartcount");
    localStorage.removeItem("cartDetails");
    localStorage.removeItem("CustomerDetails");
    localStorage.removeItem("isLoggedin");
    this.messageService.setLoginStatus(JSON.stringify("false"));
    this.messageService.setCount(JSON.stringify(0));
    this.route.navigate([""]);
  }

  redirectToOrder() {
    if (this.visible == "true") {
      this.route.navigate(["order"]);
    } else {
      alert("Login First");
    }
  }
}
