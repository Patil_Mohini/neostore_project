import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { FooterComponent } from "./footer/footer.component";
import { HeaderComponent } from "./header/header.component";

import { ProductDetailComponent } from "./product-detail/product-detail.component";
import { ProductListComponent } from "./product-list/product-list.component";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { LoginComponent } from "./login/login.component";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { RegisterComponent } from "./register/register.component";
import { OrderComponent } from "./order/order.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { NgxStarsModule } from "ngx-stars";
import { NgxStarRatingModule } from "ngx-star-rating";
import { ContactUsComponent } from "./contact-us/contact-us.component";
import { ProfileComponent } from "./profile/profile.component";
``;
import { NgxExtendedPdfViewerModule } from "ngx-extended-pdf-viewer";
import { RatingModule } from "ng-starrating";

import { SubscribeComponent } from "./subscribe/subscribe.component";
import { NgxSpinnerModule } from "ngx-spinner";
import { LoaderComponent } from "./loader/loader.component";
import { LoaderService } from "./loader.service";

import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { LoaderInterceptor } from "./loader.interceptor";
import { JwPaginationComponent } from "jw-angular-pagination";
import { AlertComponent } from "./alert/alert.component";
import { CartComponent } from "./cart/cart.component";
import { PinchZoomModule } from "ngx-pinch-zoom";
import { RecoverPasswordComponent } from "./recover-password/recover-password.component";

import { DialogBoxComponent } from "./dialog-box/dialog-box.component";
import { AddressComponent } from "./address/address.component";
import { MyAccountComponent } from "./my-account/my-account.component";
import { ChangepasswordComponent } from "./changepassword/changepassword.component";
import { MaterialUI } from "./materialUI.module";
import { OrderPlacedComponent } from './order-placed/order-placed.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    ProductDetailComponent,
    ProductListComponent,
    LoginComponent,
    RegisterComponent,
    OrderComponent,
    DashboardComponent,
    ContactUsComponent,
    ProfileComponent,
    SubscribeComponent,
    LoaderComponent,
    JwPaginationComponent,
    AlertComponent,
    CartComponent,
    RecoverPasswordComponent,
    DialogBoxComponent,
    AddressComponent,
    MyAccountComponent,
    ChangepasswordComponent,
    OrderPlacedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    FormsModule,
    NgxSpinnerModule,
    HttpClientModule,
    NgxStarsModule,
    RatingModule,
    NgxExtendedPdfViewerModule,
    BrowserAnimationsModule,
    NgxStarRatingModule,
    PinchZoomModule,
    MaterialUI
  ],
  entryComponents: [DialogBoxComponent],
  //providers: [{provide: APP_BASE_HREF, useValue: '/app'}],
  providers: [
    LoaderService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true }
  ],

  bootstrap: [AppComponent]
})
export class AppModule {}
